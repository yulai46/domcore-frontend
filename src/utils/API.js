import axios from "axios";
export const MAIN_URL = 'http://dc.websalamat.ru/';
export default axios.create({
   baseURL: `${MAIN_URL}v1/`,
   responseType: "json",
});
