export const noticeConstants = {
   SET_QUESTIONS: 'SET_QUESTIONS',
   SET_REQUEST: 'SET_REQUEST',
   SET_REVIEWS: 'SET_REVIEWS',
};
