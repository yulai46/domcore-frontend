export const reviewsConstants = {
   SET_ERROR: 'SET_ERROR',
   SET_REFRESH : 'SET_REFRESH',
   SET_ITEMS: "SET_ITEMS",
   SET_THEMES: "SET_THEMES",
   SET_DETAIL: "SET_DETAIL",
};
