export const emplorerConstants = {
   SET_FLOOR: 'SET_FLOOR',
   CLEAR_FLOOR : 'CLEAR_FLOOR',
   SET_TPL_APART: 'SET_TPL_APART',
   CLEAR_TPL_APART: 'CLEAR_TPL_APART'
};
