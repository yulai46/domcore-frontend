export const newsConstants = {
  SET_NEWS: 'SET_NEWS',
  SET_ERROR: 'SET_ERROR',
  SET_CURRENT_NEWS: 'SET_CURRENT_NEWS',
};
