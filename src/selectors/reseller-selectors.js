export const getResellerObjectsSelect = state => state.reseller.data;
export const getResellerIsLoading = state => state.reseller.isLoading;
export const getCurrentResellerObject = state => state.reseller.currentObject;
export const getResellerFilter = state => state.reseller.filter;
