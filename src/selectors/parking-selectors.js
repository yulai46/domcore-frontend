export const getParkingListsSelect = state => state.parking.data;
export const getParkingSelect = state => state.parking.parking;

export const getParkingLevelsListSelect = state => state.parking.levels;
export const getParkingLevelSelect = state => state.parking.level;

export const getParkingPlacesListSelect = state => state.parking.places;
export const getParkingPlaceSelect = state => state.parking.place;
