export const getCitiesForFormSelector = (state) => {
   const newArr = [];
   state.city.data.forEach((val) => {
      newArr.push({ value: val.id, label: val.name });
   });
   return newArr;
};
