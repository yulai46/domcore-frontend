import React, {useState} from "react";
import {IconPlus} from "../Icons/IconPlus";
import {useSelector} from "react-redux";

export const RenderImageFromString = ({
                               input: {onChange, onBlur, ...inputProps},
                               setMediaData,
                               ...props
                            }) => {
   const [file, setFile] = useState({file: null});
   const qr = useSelector((state) => state.form['city-setting'].values.qr !== 'null' || false);

   const handleChange = (e) => {
      const firstFile = e.target.files[0]
      const file = URL.createObjectURL(firstFile);
      const fileReader = new FileReader();
      fileReader.readAsArrayBuffer(firstFile);
      fileReader.onload = () => {
         const arrayBuffer = fileReader.result;
         setMediaData(arrayBuffer)
      }
      setFile({file});
   };

   return (
      <div
         className={
            props.dop
               ? "preview preview--min flex flex--ac flex--jc-center"
               : "preview flex flex--ac flex--jc-center"
         }
      >
         {file.file === null || !qr ? (
            <div className="preview__txt">
               <IconPlus/>
            </div>
         ) : (
            <img src={qr ? 'http://dc.websalamat.ru/img/'+qr : file.file} alt="" className="preview__view"/>
         )}
         <input
            {...inputProps}
            onChange={(e) => handleChange(e)}
            type="file"
            accept="image/*"
            {...props}
         />
      </div>
   );
};
