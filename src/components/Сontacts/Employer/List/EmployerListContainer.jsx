import React, {useEffect} from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { HideModal } from "../../../Modal/HideModal";
import {EmployerList} from "./EmployerList";
import {useDispatch} from "react-redux";
import {receiveWorkerDetails} from "../../../../actions/workerActions";

export const EmployerListContainer = () => {
   let { url, params: {id} } = useRouteMatch();
   const dispatch = useDispatch();

   const [open, setOpen] = React.useState(false);

   const handleOpenModal = () => {
      setOpen(true);
   };

   useEffect(() => {
      dispatch(receiveWorkerDetails(id))
   }, [id, dispatch]);

   return (
      <>
         <div className="group min-mb manager-control ">
            <Link to={`${url}/employer/create`} className="btn btn--green">
               + Добавить сотрудника
            </Link>
         </div>
         <div className="card card--width">
            <ul className="list-none list-sub">
               <EmployerList url={url} />
            </ul>
         </div>
         <HideModal open={open} setOpen={setOpen} />
      </>
   );
};
