import React from "react";
import {Link} from "react-router-dom";
import {ButtonDelete} from "../../../Buttons/ButtonDelete";
import {ButtonVisibility} from "../../../Buttons/ButtonVisibility";

export const EmployerList = ({ data, url, handleOpenModal }) => {
   return (
      <li className="list-sub__item flex flex--sb">
         <Link className="list-sub__link" to={`${url}/1/employer`}>
            Александров Евгений Викторович
         </Link>
         <div className="group">
            <ButtonDelete w={22} h={22} />
            <Link
               to="#"
               className="btn-option  btn-reset"
               title="Редактировать"
               aria-label="Редактировать"
            >
               <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="22"
                  height="22"
                  viewBox="0 0 22 22"
                  fill="none"
               >
                  <path
                     d="M16.6 1L1.51429 16.3936L1 21L5.91429 20.8834L21 5.4898L16.6 1Z"
                     stroke="black"
                  />
               </svg>
            </Link>
            <ButtonVisibility onClick={handleOpenModal} />
         </div>
      </li>
   )
}
