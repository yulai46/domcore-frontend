import React from "react";
import { Header } from "../../Header/Header";
import { BasisInformation } from "../BasisInformation/BasisInformation";
import { EmployerListContainer } from "../Employer/List/EmployerListContainer";
import { ManagersContainer } from "../Managers/ManagersContainer";
import { TabsContent } from "../../Tabs/TabsContent";

export const UpdateCity = () => {
   const tabs = [
      {
         label: "Общая",
         components: <BasisInformation />,
      },
      {
         label: "Сотрудники",
         components: <EmployerListContainer />,
      },
      {
         label: "Менеджеры",
         components: <ManagersContainer />,
      },
   ];

   return (
      <>
         <Header title="Редактировать Набережные челны" />

         <TabsContent tabs={tabs} activeTab={0} />
      </>
   );
};
