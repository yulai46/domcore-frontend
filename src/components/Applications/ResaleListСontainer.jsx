import React, {useEffect, useState} from "react";
import { Header } from "../Header/Header";
import { TableList } from "../TableList/TableList";
import {Pagination} from "../Pagination/Pagination";
import API from "../../utils/API";
import {dateFormat} from "../../utils/helpers";

export const ResaleContainer = () => {
   const [list, setList] = useState({
      items: []
   });

   let head = [
      "ID",
      "Тип заявки",
      "Имя, Тел",
      "Город",
      "Район",
      "S общ",
      "Цена",
      "Адресс",
      "Дата заявки",
      "Ссылки",
   ];

   const getList = (page = 1) => {
      API.get(`requestresale`, {params: {page}})
         .then(({data}) => {
            let dataList = data.items.map(item => ({
               id: item.id,
               type: item.type,
               name: item.name + ',' + item.phone,
               city: item.city,
               area: item.area,
               total_area: item.total_area,
               price: item.price,
               address: item.address,
               date: dateFormat(item.date_create, "YYYY-MM-DD, h:mm"),
               links: {
                  delete: true,
               },
            }));
            setList({
               meta: data._meta,
               items: dataList
            });
         });
   }

   const handleDelete = (e) => (dispatch) => {
      API.delete(`requestresale/delete/${e}`)
         .then((r) => {
            if(r)
               getList();
         });
   }

   useEffect(() => {
      getList();
   },[]);

   return (
      <>
         <Header title="Вторичка: обратный звонок, заявка на просмотр" />
         <div className="card">
            <TableList handleDelete={handleDelete} head={head} list={list.items} />
            {
               list.meta &&
               <Pagination
                  onSetPage={getList}
                  pageCount={list.meta.pageCount}
                  currentPage={list.meta.currentPage}
                  itemCount={list.items.length}
                  totalCount={list.meta.totalCount}
               />
            }
         </div>
      </>
   );
};
