import React, {useEffect, useRef, useState} from 'react';
import {Stage, Layer, Line} from 'react-konva';
import {BaseImage} from "../General/BaseImage";
import Portal from "../General/Portal";
import {CirclePoint} from "./CirclePoint";
import {ContextMenu} from "./ContextMenu";
import {useCheckSize} from "../General/useCheckSize";
import {getRelativePointerPosition} from "../General/getRelativePointerPosition";
import {ChangeText} from "./ChangeText";
import {ZoomStage} from "../General/ZoomStage";

let circleId = 1;

export const CanvasUpdateWithoutBlank = ({ image = false, handleSave, pointerObject = false }) => {
   const [size, setSize] = useCheckSize(); // размеры
   const [scale, setScale] = useState(1); // маштаб

   const stageRef = useRef();
   const [points, setPoint] = useState([]);
   const [closed, setClosed] = useState(false);
   const [isDrawing, setDrawing] = useState(true);

   //closing drawing
   const handleClick = (id) => {
      if (id === 1) {
         circleId = 1;
         setClosed(true);
         setDrawing(false)
      }
   };

   const handleDragMove = (x, y, id) => {
      const newPoints = points.map(p => {
         if (p.id === id){
            return {
               id: p.id,
               x,
               y
            }
         }
         return p;
      });
      setPoint(newPoints);
   };

   /* context menu */
   const [contextMenu, setContextMenu] = useState(null);
   const handleContextMenu = (e) => {
      e.evt.preventDefault(true);
      const rect = e.target.getStage().container().getBoundingClientRect();
      const mousePosition = {
         x: rect.left+ window.scrollX + e.target.getStage().getPointerPosition().x + 'px',
         y: rect.top +  window.scrollY+ e.target.getStage().getPointerPosition().y + 'px'
      };
      setContextMenu(mousePosition)
   };

   const deleteShape = () => {
      setPoint([]);
      setContextMenu(null);
      setDrawing(true);
      setClosed(false);
      circleId = 1;
   };

   /* edit text and drag*/
   const [nameValue, setNameValue] = useState('Название');
   const [editVisible, setEditVisible] = useState(false);
   const [positionTextarea, setPositionTextarea] = useState({textX: 0, textY: 0});
   const [positionText, setPositionText] = useState({x: 50, y: 20});

   //Добавим точки к дому на редактирование
   useEffect(() => {
      if(pointerObject)
      {
         if(pointerObject.popup)
            setPositionText(pointerObject.popup.position);
            setNameValue(pointerObject.popup.title);

         if(pointerObject.points)
            setPoint(pointerObject.points);
      }
   },[pointerObject]);

   const saveShape = (e) => {
      setContextMenu(null);
      handleSave({
            points: points,
            popup: {}
      });
   };

   const handleTextDblClick = (e) => {
      let position = e.target.getAbsolutePosition();
      const rect = e.target.getStage().container().getBoundingClientRect();

      setEditVisible(true);
      setPositionTextarea({textX: rect.left + position.x, textY: rect.top + position.y})
   };

   const handleDragMoveText = (e) => {
      setPositionTextarea({textX: e.target.x(), textY: e.target.y()});
      setPositionText({x: e.target.x(), y: e.target.y()})
   };

   const handleTextEdit = (e) => {
      setNameValue(e.target.value);
   };

   const handleTextareaKeyDown = (e) => {
      if (e.keyCode === 13 && !e.shiftKey) {
         setEditVisible(false);
      }

      //escape
      if (e.keyCode === 27) {
         setEditVisible(false);
      }
   };

   const zoomStage = (stage, scaleBy) => {
      ZoomStage(stage, scaleBy)
   };

   return (
      <>
         <div className="frame-canvas">
            <Stage
               ref={stageRef}
               width={size.width}
               height={size.height}
               scaleX={scale}
               scaleY={scale}
               className="canvas"
               draggable={true}
               onClick={e => {
                  if(isDrawing) {
                     const pointStart = getRelativePointerPosition(e.target.getStage());

                     const point = {
                        id: circleId++,
                        x: pointStart.x,
                        y: pointStart.y
                     };

                     setPoint([...points, point]);
                  }
               }}
            >
               <Layer>
                  <BaseImage photo={image} size={size} setScale={setScale} setSize={setSize} />
               </Layer>

               {points.length ?
                  <Layer>
                     <Line
                        points={points.flatMap(p => [p.x, p.y])}
                        stroke="red"
                        closed = {closed}
                        fill = '#e0f5c0'
                        opacity={0.5}
                        onContextMenu={handleContextMenu}
                     />

                     {points.map((point, index) => (
                        <CirclePoint
                                     key={index}
                                     point={point}
                                     handleClick={handleClick}
                                     handleDragMove={handleDragMove}
                                     isDrawing={isDrawing}
                        />
                     ))}


                     <Portal>
                        {contextMenu &&
                           <ContextMenu
                              {...contextMenu}
                              deleteShape={deleteShape}
                              saveShape={saveShape}
                           />
                        }

                        {closed &&
                           <ChangeText
                              value={nameValue}
                              handleTextEdit={handleTextEdit}
                              handleTextareaKeyDown={handleTextareaKeyDown}
                              position={positionTextarea}
                              editVisible={editVisible}
                           />
                        }
                     </Portal>
                  </Layer>
                  : <></>
               }
            </Stage>
            <div className="zoom-container">
               <button
                  onClick={() => {
                     zoomStage(stageRef.current, 1.2);
                  }}
               >
                  +
               </button>
               <button
                  onClick={() => {
                     zoomStage(stageRef.current, 0.8);
                  }}
               >
                  -
               </button>
            </div>
         </div>
      </>
   )
};
