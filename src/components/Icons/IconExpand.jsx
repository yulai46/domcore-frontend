import React from "react";

export const IconExpand = ({ onClick }) => (
   <svg width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={onClick} >
      <path d="M1 1L7 7C9 5 11.5 2.5 13 1" stroke="black" strokeWidth="2"/>
   </svg>
);
