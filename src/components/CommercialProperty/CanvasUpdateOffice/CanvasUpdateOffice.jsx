import React, { useEffect, useRef, useState } from "react";
import { Stage, Layer, Line } from "react-konva";
import { Redirect, useRouteMatch } from "react-router-dom";

import { useCheckSize } from "../../Canvas/General/useCheckSize";
import API from "../../../utils/API";
import Image from "../../../assets/img/canvas-1.png";
import { ZoomStage } from "../../Canvas/General/ZoomStage";
import { getRelativePointerPosition } from "../../Canvas/General/getRelativePointerPosition";
import { BaseImage } from "../../Canvas/General/BaseImage";
import { CirclePoint } from "../../Canvas/CanvasCreate/CirclePoint";
import { Title } from "../../Canvas/CanvasCreate/Title";
import Portal from "../../Canvas/General/Portal";
import { ContextMenu } from "../../Canvas/CanvasCreate/ContextMenu";
import { ChangeText } from "../../Canvas/CanvasCreate/ChangeText";

let circleId = 1;

export const CanvasUpdateOffice = () => {
   const [size, setSize] = useCheckSize(); // размеры
   const [scale, setScale] = useState(1); // маштаб
   const [image, setImage] = useState(Image);
   const stageRef = useRef();
   const [points, setPoint] = useState([]);
   const [closed, setClosed] = useState(false);
   const [isDrawing, setDrawing] = useState(true);
   const match = useRouteMatch();
   const [offices, setOffices] = useState([]);

   useEffect(() => {
      API.get(
         `media?parent=comrealty&id_parent=${match.params.id}&type=plan`
      ).then(({ data }) => {
         setImage(`http://dc.websalamat.ru/img/${data.items[0].image}`);
      });
   }, [match.params.id]);

   useEffect(() => {
      API.get(`comrealty/view/${match.params.id}`).then(({ data }) => {
         setOffices(data.offices);
      });
   }, [match.params.id]);

   //closing drawing
   const handleClick = (id) => {
      if (id === 1) {
         setClosed(true);
         setDrawing(false);
      }
   };

   const handleDragMove = (x, y, id) => {
      const newPoints = points.map((p) => {
         if (p.id === id) {
            return {
               id: p.id,
               x,
               y,
            };
         }
         return p;
      });
      setPoint(newPoints);
   };

   /* context menu */
   const [contextMenu, setContextMenu] = useState(null);
   const handleContextMenu = (e) => {
      e.evt.preventDefault(true);
      const rect = e.target.getStage().container().getBoundingClientRect();
      const mousePosition = {
         x:
            rect.left +
            window.scrollX +
            e.target.getStage().getPointerPosition().x +
            "px",
         y:
            rect.top +
            window.scrollY +
            e.target.getStage().getPointerPosition().y +
            "px",
      };
      setContextMenu(mousePosition);
   };

   const deleteShape = () => {
      setPoint([]);
      setContextMenu(null);
      setDrawing(true);
      setClosed(false);
      circleId = 1;
   };

   /* edit text and drag*/
   const [nameValue, setNameValue] = useState("Название");
   const [editVisible, setEditVisible] = useState(false);
   const [positionTextarea, setPositionTextarea] = useState({
      textX: 0,
      textY: 0,
   });
   const [positionText, setPositionText] = useState({ x: 50, y: 20 });

   const [save, setSave] = useState(false);
   const officeId = match.params.office_id;

   //Добавим точки к дому на редактирование
   useEffect(() => {
      if (officeId && offices[officeId]) {
         const home = offices[officeId];
         const homePlan = JSON.parse(home.plan);
         if (homePlan.popup) setPositionText(homePlan.popup.position);
         setNameValue(homePlan.popup.title);

         if (homePlan.points) setPoint(homePlan.points);
      }
   }, [officeId, offices]);

   const saveShape = (e) => {
      setContextMenu(null);
      const formData = new FormData();
      formData.append("name", nameValue);
      formData.append("comrealty_id", match.params.id);
      formData.append(
         "plan",
         JSON.stringify({
            points: points,
            popup: {
               title: nameValue,
               position: {
                  x: positionTextarea.textX,
                  y: positionTextarea.textY,
               },
            },
         })
      );
      API.put(`office/update/${parseInt(officeId)}`, formData).then(() => {
         setSave(true);
      });
   };

   const handleTextDblClick = (e) => {
      let position = e.target.getAbsolutePosition();
      const rect = e.target.getStage().container().getBoundingClientRect();

      setEditVisible(true);
      setPositionTextarea({
         textX: rect.left + position.x,
         textY: rect.top + position.y,
      });
   };

   const handleDragMoveText = (e) => {
      setPositionTextarea({ textX: e.target.x(), textY: e.target.y() });
      setPositionText({ x: e.target.x(), y: e.target.y() });
   };

   const handleTextEdit = (e) => {
      setNameValue(e.target.value);
   };

   const handleTextareaKeyDown = (e) => {
      if (e.keyCode === 13 && !e.shiftKey) {
         setEditVisible(false);
      }

      //escape
      if (e.keyCode === 27) {
         setEditVisible(false);
      }
   };

   /* end */

   const zoomStage = (stage, scaleBy) => {
      ZoomStage(stage, scaleBy);
   };

   if (save)
      return <Redirect to={`/residential-complex/update/${match.params.id}`} />;

   return (
      <>
         <div className="frame-canvas">
            <Stage
               ref={stageRef}
               width={size.width}
               height={size.height}
               scaleX={scale}
               scaleY={scale}
               className="canvas"
               draggable={true}
               onClick={(e) => {
                  if (isDrawing && e.target.constructor.name === "Image") {
                     const pointStart = getRelativePointerPosition(
                        e.target.getStage()
                     );

                     const point = {
                        id: circleId++,
                        x: pointStart.x,
                        y: pointStart.y,
                     };

                     setPoint([...points, point]);
                  }
               }}
            >
               <Layer>
                  <BaseImage
                     photo={image}
                     size={size}
                     setScale={setScale}
                     setSize={setSize}
                  />
               </Layer>

               {points.length ? (
                  <Layer>
                     <Line
                        points={points.flatMap((p) => [p.x, p.y])}
                        stroke="red"
                        closed={closed}
                        fill="#e0f5c0"
                        opacity={0.5}
                        onContextMenu={handleContextMenu}
                     />

                     {points.map((point, index) => (
                        <CirclePoint
                           key={index}
                           point={point}
                           handleClick={handleClick}
                           handleDragMove={handleDragMove}
                           isDrawing={isDrawing}
                        />
                     ))}

                     {closed && editVisible === false && (
                        <Title
                           position={positionText}
                           handleDragMoveText={handleDragMoveText}
                           handleTextDblClick={handleTextDblClick}
                           title={nameValue}
                        />
                     )}

                     <Portal>
                        {contextMenu && (
                           <ContextMenu
                              {...contextMenu}
                              deleteShape={deleteShape}
                              saveShape={saveShape}
                           />
                        )}

                        {closed && (
                           <ChangeText
                              value={nameValue}
                              handleTextEdit={handleTextEdit}
                              handleTextareaKeyDown={handleTextareaKeyDown}
                              position={positionTextarea}
                              editVisible={editVisible}
                           />
                        )}
                     </Portal>
                  </Layer>
               ) : (
                  <></>
               )}
            </Stage>
            <div className="zoom-container">
               <button
                  onClick={() => {
                     zoomStage(stageRef.current, 1.2);
                  }}
               >
                  +
               </button>
               <button
                  onClick={() => {
                     zoomStage(stageRef.current, 0.8);
                  }}
               >
                  -
               </button>
            </div>
         </div>
      </>
   );
};
