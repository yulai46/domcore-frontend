import React, {useEffect, useState} from 'react';
import Konva from "konva";
import {Stage, Layer, Group} from 'react-konva';
import {BaseImage} from "../../Canvas/General/BaseImage";
import {useCheckSize} from "../../Canvas/General/useCheckSize";
import {Popup} from "./Popup";
import {Figure} from "./Figure";
import {ZoomStage} from '../../Canvas/General/ZoomStage'
import {useRouteMatch, Redirect} from "react-router-dom";

Konva.hitOnDragEnabled = true;

export const CanvasPreview = ({image , pointsObjects = false}) => {
  let { url } = useRouteMatch();

  const ref = React.useRef();
  const [size, setSize] = useCheckSize();
  const [scale, setScale] = useState(1);

  const [data, setData] = useState(false);

  useEffect(() => {
     if(pointsObjects)
     {
        const loadHouses = pointsObjects.map(item => {
           return {
              ...item,
              points: (item.plan && JSON.parse(item.plan).points) ? JSON.parse(item.plan).points : [],
              popup: (item.plan && JSON.parse(item.plan).popup) ? JSON.parse(item.plan).popup : {
                 position: {
                    x: 0,
                    y:0,
                 },
                 title: ""
              },
           }
        })
        setData(loadHouses);
     }
  },[pointsObjects]);

  const zoomStage = (stage, scaleBy) => {
     ZoomStage(stage, scaleBy)
  };

  const [redirect, setRedirect] = useState(false);

  const handleRedirect = (e) => {
     setRedirect(e)
  };

   return (
     <div className="frame flex flex--ac flex--jc-center">
         {redirect !== false &&
            <Redirect
               push
               to={{
                  pathname: `${url}/tplapart/update/${redirect}`,
               }}
            />
         }
           <div className="frame-canvas">
              <Stage
                 ref = {ref}
                 width={size.width}
                 height={size.height}
                 scaleX={scale}
                 scaleY={scale}
                 className="canvas"
                 draggable={true}
              >
                  <Layer>
                     <Group>
                        <BaseImage photo={image} size={size} setScale={setScale} setSize={setSize} />
                        {
                           data &&
                           data.map((polygon,index) => (
                              <Figure
                                 key={index}
                                 polygon={polygon}
                                 handleRedirect={() => handleRedirect(polygon.id)}
                              />
                           ))
                        }
                        {data &&
                           data.map((title,index) => {
                           const pos = {x:title.popup.position.x+17, y:title.popup.position.y+27};
                           return <Popup key={index} title={title.popup.title} position={{x: pos.x, y:pos.y}}/>
                        })}
                     </Group>
                  </Layer>
              </Stage>
              <div className="zoom-container">
                 <button
                    onClick={() => {
                       zoomStage(ref.current, 1.2);
                    }}
                 >
                    +
                 </button>
                    <button
                       onClick={() => {
                          zoomStage(ref.current, 0.8);
                       }}
                    >
                       -
                    </button>
              </div>
           </div>
        </div>
  )
};




