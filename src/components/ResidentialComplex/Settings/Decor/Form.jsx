import React, {useEffect} from "react";
import { Field, reduxForm } from "redux-form";
import { WYSIWYGEditor } from "../../../Fields/WYSIWYGEditor";
import { ButtonBack } from "../../../Buttons/ButtonBack";
import { ButtonSave } from "../../../Buttons/ButtonSave";
import { LayoutFormBasis } from "../../../Fields/LayoutForm";
import {useDispatch} from "react-redux";
import {loadFinishingById} from "../../../../actions/complexActions";
import {useRouteMatch} from "react-router-dom";

export const Form = ({ handleSubmit, initialize, setUpdate }) => {
   const dispatch = useDispatch();
   const { params }  = useRouteMatch();
   const homeId = params.home_id;

   useEffect(() => {
      (async () => {
         if(homeId)
         {
            const result = await dispatch(loadFinishingById(homeId, initialize));
            setUpdate(result);
         }
      })();
   }, [homeId, dispatch]);

   return (
      <form className="form" onSubmit={handleSubmit}>
         <LayoutFormBasis label={false} id="description" title="Описание">
            <div className="form__item form__item--all">
               <Field
                  component={WYSIWYGEditor}
                  id="description"
                  name="text"
                  type="text"
               />
            </div>
         </LayoutFormBasis>

         <LayoutFormBasis title="Опубликовано" id="published">
            <div className="form__item">
               <Field
                  name="published"
                  id="published"
                  component="input"
                  type="checkbox"
               />
            </div>
         </LayoutFormBasis>

         <div className="form__foot button-group flex">
            <ButtonBack />
            <ButtonSave />
         </div>
      </form>
   );
}

export default reduxForm({
   form: "decor-form"
})(Form);
