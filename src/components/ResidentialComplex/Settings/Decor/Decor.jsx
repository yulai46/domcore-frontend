import React, {useState} from "react";
import Form from "./Form";
import {useDispatch} from "react-redux";
import {updateFinishing} from "../../../../actions/complexActions";
import {useRouteMatch} from "react-router-dom";

export const Decor = () => {
   const dispatch = useDispatch();
   const { params }  = useRouteMatch();
   const homeId = params.home_id;
   const [update, setUpdate] = useState(false);

   const handleSubmit = (values) => {
      dispatch(updateFinishing(homeId, values, update));
   };

   return (
      <Form
         setUpdate={(update) => setUpdate(update)}
         onSubmit={(values) => {
            handleSubmit(values);
         }}
      />
   );
};
