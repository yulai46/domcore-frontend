import React, {useEffect, useState} from "react";
import DesignForm from "./DesignForm";
import {useDispatch, useSelector} from "react-redux";
import {useRouteMatch} from "react-router-dom";
import {updateDesign} from "../../../../actions/complexActions";

export const Design = ({ setActiveTab }) => {
   const [files, setFiles] = useState([]);
   const dispatch = useDispatch();
   const match  = useRouteMatch();
   const homeId = match.params.home_id;
   const house = useSelector(state => state.complex.house);

   const handleSubmit = (values) => {
      dispatch(updateDesign(homeId, files, values));
      setActiveTab(0);
   };

   useEffect(
      () => () => {
         files.forEach((file) => URL.revokeObjectURL(file.preview));
      },
      [files]
   );

   useEffect(() => {
      if(house.imagesDesign)
         setFiles(house.imagesDesign);
   },[house]);

   return (
      <DesignForm
         handleSetFiles={setFiles}
         files={files}
         onSubmit={(values) => {
            handleSubmit(values);
         }}
      />
   );
};
